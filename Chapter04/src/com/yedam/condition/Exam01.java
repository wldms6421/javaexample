package com.yedam.condition;

import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) {
		/*
		int score = (int)(Math.random()* 60) + 40;
		//Math.random의 범위는 0<=M <1; 
		//0*60 <= M* 60 <1*60
		//0<= M <60;   40~99;
		//실수 > 정수 , casting시키면 쉽게 바꿀수 ㅇ 
		
		if(score>=60) {
			System.out.println("합격");
		}else {
			System.out.println("불합격");
		}
		
		//삼항연산자
		String pass = (score>=60) ? "합격" : "불합격";
		System.out.println(pass);
		
		System.out.println(score);
		if(score>=90) {
			System.out.println("A");
		}else if(score>=80) {
			System.out.println("B");		
		}else if(score>=70) {
			System.out.println("C");
		}else {System.out.println("D");}
		
		
		//if 문을 활용한 예제
		//사용자가 입력한 값이 1~9 이면 "one","two"...
		//출력하는 프로그램을 작성하고 1~9 사이가 아니면 other
		
		System.out.print("숫자를입력하세요>" );
		Scanner sc = new Scanner(System.in);
		int no = Integer.parseInt(sc.nextLine());
		//String i = sc.nextLine();
		//int num1 = Integer.parseInt(i);
		//int num2 = (int)(Math.random()*10); 필요없음
		//String number = "";
		
		if(num1 == 1) {
			System.out.println("one");
			//number = "one";
		}else if(num1 ==2) {
			System.out.println("two");
		}else if(num1 ==3) {
			System.out.println("three");
		}else if(num1 ==4) {
			System.out.println("four");
		}else if(num1 ==5) {
			System.out.println("five");
		}else if(num1 ==6) {
			System.out.println("six");
		}else if(num1 ==7) {
			System.out.println("seven");
		}else if(num1 ==8) {
			System.out.println("eight");
		}else if(num1 ==9) {
			System.out.println("nine");
		}else {
			System.out.println("범위가 맞지 않습니다.");
		}
		
		switch(no) {
		case 1:
			System.out.println("one");
			break;
		case 2:
			System.out.println("two");
			break;
		case 3:
			System.out.println("three");
			break;
		case 4:
			System.out.println("four");
			break;
		case 5:
			System.out.println("five");
			break;
		case 6:
			System.out.println("six");
			break;
		case 7:
			System.out.println("seven");
			break;
		case 8:
			System.out.println("eight");
			break;
		case 9:
			System.out.println("nine");
			break;
		default:
			System.out.println("other");
			break;
		}
		
		
		
		//break 없는 switch문
		int time = (int)(Math.random() * 4) + 8;
		System.out.println("현재 시각 : " + time + "시");
		
		switch(time) {
		case 8:
			System.out.println("출근을 합니다.");
		case 9:
			System.out.println("회의을 합니다.");
		case 10:
			System.out.println("업무를 봅니다.");
		default:
			System.out.println("외근을 합니다.");
		}
		*/
		
		//switch문으로 성적확인, case 문자도 올 수 있음.
		//문자열도 가능
		char grade = 'B';
		
		switch(grade) {
		case 'A':
			System.out.println("훌륭한 학생입니다.");
			break;
		case 'B':
			System.out.println("우수한 학생입니다.");
			break;
		case 'C':
			System.out.println("조금 노력하세요.");
			break;
		case 'D':
			System.out.println("분발 하세요.");
			break;
		}
		
		
		//문자열을 활용한 switch문
		
		String position = "부장";
		switch(position) {
		case "부장":
			System.out.println("700만원");
			break;
		case "차장":
			System.out.println("600만원");
			break;
		case "과장":
			System.out.println("500만원");
			break;
		default:
			System.out.println("300만원");
			break;
		}
		
		
		
		
		
	}//main
}//class
