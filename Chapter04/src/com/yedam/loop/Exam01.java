package com.yedam.loop;

public class Exam01 {
	public static void main(String[] args) {
		/*1) 규칙찾기
		int sum = 0;
		sum = sum +1;
		sum = sum +2;
		sum = sum +3;
		sum = sum +4;
		sum = sum +5;
		
		
		
		//1~5 까지의 합을 구하는 반복문
		int sum = 0;
		for(int i =1; i<=5; i++) {
			sum = sum + i;
			System.out.println(sum);
		}
		System.out.println(sum);
		
		
		
		//짝수 구하는 반복문 
		//규칙 2,4,6,8,10    1~100사이의
		for(int i=1; i<=100; i++) {
			if(i%2 ==0) {
				System.out.println(i);
			}//if
		}//for
		*/
		
		
		/*
		//1~100사이에서 홀수 구하는 반복문
		//규칙   100에서 내려오게도 할 수 있음
		for(int i=100; i>=1; i--) {
			if(i%2 ==1) {
				System.out.println(i);
			}//if
		}//for
		
		
		
		//1~100 2의 배수 또는 3의 배수 찾기 ||
		//2의 배수 이거나 3의 배수 찾기 &&  -> 6의 배수
		
		for(int i=1; i<=100; i++) {
			if(i%2==0 || i%3==0) {
				System.out.println(i + "는 2의 배수 또는 3의 배수입니다.");
			}
			
		}//for
		
		for(int i=1; i<=100; i++) {
			if(i%2==0 && i%3==0) {
				System.out.println(i + "는 2의 배수 이고 3의 배수입니다.");
			}
			
		}//for
		
		
		
		//구구단 출력
		//만약 2단출력
		for(int i=1; i<=9; i++) {
			
			System.out.println("2 x "+i+" = " + (2*i));
		}
		
		
		//2~9단 출력
		for(int i=2; i<=9; i++) {
			//i 2일 때 j 는 9번 돌아감
			for(int j=1; j<=9; j++) {
				System.out.println(i + "*" + j + "=" + (i*j));	
			}//for
		}//for
		
		
		
		//별찍기
		
		//*****
		//*****
		//*****
		//*****
		//*****
		
		//한칸씩 내려갈때 사용
		for(int i=0; i<5; i++) {
			String star = "";
			//별만드는 반복문
			for(int j =0; j<5; j++) {
				star = star + "*";
				//연산자를 활용하여 *****를 만듬.
			}
			System.out.println(star);
		}
		
		
		//*
		//**
		//***
		//****
		//*****
		
		for(int i=1; i<=5; i++) {
			String star = "";
			//별만드는 반복문
			for(int j =0; i>j; j++) {
				star += "*";
				//연산자를 활용하여 *****를 만듬.
			}
			System.out.println(star);
		}
		
		
		
		for(int i=0; i<5; i++) {
			String star = "";
			for(int j=i; j>=0; j--) {
				
				star += "*";
			}
			System.out.println(star);
		}
		*/
		
		//*****
		//****
		//***
		//**
		//*
		
		for(int i=1; i<=5; i++) {
			String star = "";
			//별만드는 반복문
			for(int j =i; j<=5; j++) {
				star += "*";
				//연산자를 활용하여 *****를 만듬.
			}
			System.out.println(star);
		}
		
		
		
		//    * 4,1;
		//   ** 3,2;
		//  *** 2,3
		// **** 1,4
		//***** 0,5
		
		for(int i=5; i>0; i--) {
			String star = "";
			//별만드는 반복문
			for(int j =0; j<=5; j++) {
				if(j<i) {
					star += " ";
				}else if(j>=i) {
					star += "*";
				//연산자를 활용하여 *****를 만듬.
				}//if
			}
			System.out.println(star);
		}
		
		
		
		
		
		
		
	}//main
}//class
