package com.yedam.loop;

import java.util.Scanner;

public class Exam02 {
	public static void main(String[] args) {
		
		//int i =1;
		//int sum = 0;
		//while(i<=5) {
		//	sum=sum +i;
		//	System.out.println(i);
		//	i++;	
		//}
		
		//while(i<=5 && i<10)
		/*
		while(i<=100) {
			if(i%2 == 0) {
				System.out.println(i);
			}
			i++;
			
		}
		
		
		boolean flag = true;
		i=0;
		while(flag) {
			if(i==50) {
				//break;
				flag = false;
			}
			i++;
		}
		System.out.println("end of prog");
		
		
		//계산기 프로그램
		
		boolean flag = true;
		Scanner sc = new Scanner(System.in);
		while(flag) {
			System.out.println("1. 더하기 | 2. 빼기 | 3. 곱하기 | 4. 종료");
			System.out.println("입력>");
			//입력한 데이터를 받는 변수
			int no = Integer.parseInt(sc.nextLine());
			
			switch(no) {
			case 1:
				System.out.println("더하고자 하는 두 수를 입력하세요.");
			    System.out.println("1>");
			    int num = Integer.parseInt(sc.nextLine());
			    System.out.println("2>");
			    int num2 = Integer.parseInt(sc.nextLine());
			    System.out.println(num +" + " + num2 + "의 결과 : "+ (num+num2));
				break;
			case 2:
				System.out.println("빼고자 하는 두 수를 입력하세요.");
			    System.out.println("1>");
			    num = Integer.parseInt(sc.nextLine());
			    System.out.println("2>");
			    num2 = Integer.parseInt(sc.nextLine());
			    System.out.println(num +" - " + num2 + "의 결과 : "+ (num-num2));
				break;
			case 3:
				System.out.println("곱하고자 하는 두 수를 입력하세요.");
			    System.out.println("1>");
			    num = Integer.parseInt(sc.nextLine());
			    System.out.println("2>");
			    num2 = Integer.parseInt(sc.nextLine());
			    System.out.println(num +" * " + num2 + "의 결과 : "+ (num*num2));
				break;
			case 4:
				System.out.println("종료합니다.");
				flag = false;
			
			default:
				System.out.println("번호를 잘못 입력 하셨습니다.");
				break;
			}
		}//while
		
		*/
		
		//game 만들기
		//while 문 이용
		//가위 바위 보
		//동전 앞뒤맞추기
		
		boolean flag = true;
		int money;
		System.out.println("====insert Coin====");
		
		
		while(flag) {
			Scanner sc = new Scanner(System.in);
			money= Integer.parseInt(sc.nextLine());
			//한판에 500원
			System.out.println((money/500) + "번의 기회가 있습니다.");
			System.out.println("1. 가위바위보 | 2. 앞 뒤 맞추기 | 3. 종료");
			System.out.println("입력>");
			int gameNo = Integer.parseInt(sc.nextLine());
			
			while(true) {
			if(gameNo == 1) {
				System.out.println("가위, 바위, 보 중에서 하나를 입력하세요.");
				//사용자
				String RSP = sc.nextLine();
				//컴퓨터
				int randomNo = (int)(Math.random() * 3) +1;
				//1~3 사이의 정수
				if(RSP.equals("가위")) {
						if(randomNo == 1) {
							System.out.println("비겼다.");
						}else if (randomNo == 2) {
							System.out.println("졌다.");
						}else {
							System.out.println("이겼다.");
						}
				}else if(RSP.equals("바위")) {
					if(randomNo == 1) {
						System.out.println("이겼다.");
					}else if (randomNo == 2) {
						System.out.println("비겼다.");
					}else {
						System.out.println("졌다.");
					}
					
				}else if(RSP.equals("보")) {
					if(randomNo == 1) {
						System.out.println("졌다.");
					}else if (randomNo == 2) {
						System.out.println("이겼다.");
					}else {
						System.out.println("비겼다.");
					}
				money -= 500;
				
				
			}else if(gameNo == 2) {
				
				System.out.println("앞, 뒤 중에서 하나를 입력하세요.");
				System.out.println("입력>");
				String FB = sc.nextLine();
				int randomNo2 = (int)(Math.random() * 2) +1;
				
				if(FB.equals("앞")) {
					if(randomNo2 == 1) {
						System.out.println("맞습니다.");
					}else if(randomNo2 == 2) {
						System.out.println("아쉽네요.");
					}
				}else if(FB.equals("뒤")) {
					if(randomNo2 == 1) {
						System.out.println("아쉽네요.");
					}else if(randomNo2 == 2) {
						System.out.println("맞습니다.");
					}
					
				}
				money -= 500;
				
				
			}   else if(gameNo == 3) {
				System.out.println("종료합니다.");
				break;
			} else if (money == 0) {
				System.out.println("Coin이 부족합니다.");
				break;
			}
			
				
				
			}
				
				
			
			
		}//while
		
			
			
				
				
				
		
		
		
		
		
		
		}	
	}//main
	}//class
