package com.yedam.inout;

import java.io.IOException;
import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) throws Exception {
		int value = 123;
		String name = "상품";
		double price = 1000.10;
		System.out.printf("상품의 가격 : %d\n", value);
		//123 출력
		System.out.printf("%s의 가격 : %d\n", name, value);
		System.out.printf("%s의 가격 : %d원, %f\n", name, value, price);
		
		//1)정수 사용
		value = 11;
		System.out.printf("%d\n", value);
		System.out.printf("%6d\n", value);
		System.out.printf("%-6d\n", value);
		System.out.printf("%06d\n", value);
		
		//2)실수 사용
		price = 123.45;
		System.out.printf("%f\n", (double)value/5);  //double로 바꾸어서 표현할 수 있음.
		System.out.printf("%10.2f\n", price);
		System.out.printf("%-10.2f\n", price);
		System.out.printf("%010.2f\n", price);
		
		//3)문자열 사용
		System.out.printf("%s\n" , "문자열사용");
		System.out.printf("%6s\n" , "문자열사용");
		System.out.printf("%-6s\n" , "문자열사용");
		
		System.out.print("아무것도 없는 printf");  //기본 출력문
		
		//입력
		//System.in.read(); 2개이상 한글 안됨, 문자열 안됨
		//대체하기 위해 Scanner 사용
		//System.out.println("원하는 값 입력 : ");
		//int keyCode = System.in.read();
		//System.out.println("keycode : "+ keyCode);
		//keyCode = System.in.read();
		//System.out.println("keycode : "+ keyCode);
		//keyCode = System.in.read();
		//System.out.println("keycode : "+ keyCode);
		
		
		//스캐너(Scanner)
		Scanner sc = new Scanner(System.in);
		System.out.println("데이터입력> ");
		String word = sc.nextLine();   //문자열 입력 숫자도 문자열로, parseInt 쓰면 됨.
		System.out.println(word);
		
		// == 문자열로는 equals()로 비교해야함.
		if(word.equals("test")){
				System.out.println("equals: 입력하신 문자열과 비교 문자열이 같음.");
		}
		if(word == "test") {
			System.out.println("== : 입력하신 문자열과 비교 문자열이 같음");
		}
		
	}}
