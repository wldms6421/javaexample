package com.yedam.variable;

public class Exam03 {
	public static void main(String[] args) {
		//자동타입변환
		char cVar = 'A'+1;    // 65+1
		int IVar = cVar;
		System.out.println(IVar);   //66 출력
		
		double dVar = IVar;
		System.out.println(dVar);   //66.0
		System.out.println();
		//큰 값 -> 작은 값 강제 타입 변환. 작은값 이후는 다 잘림
		//강제 타입변환
		IVar = (int)dVar;
		System.out.println(IVar);  //66
		
		cVar = (char)IVar;
		System.out.println(cVar);  //B
		
		double dVar2 = 3.14444;
		int iVar2 = (int)dVar2;
		System.out.println(iVar2);   //3 
		
		byte result = 10 + 20;
		System.out.println(result);
		
		byte x = 10;
		byte y = 20;
		//int result2 = x + y;        30  강제 타입 변환 활용
		byte result3 = (byte)(x + y);
		
		
		//데이터 타입 크기에 따른 연산
		// long + int = long 
		// byte + int = int
		byte bVar = 10;
		int iVar1 = 100;
		long lVar = 1000L;
		int result4 = (int)(bVar+iVar1+lVar);
		System.out.println(result4);
		
		
		
	}
}
