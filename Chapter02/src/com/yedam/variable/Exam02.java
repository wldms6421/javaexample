package com.yedam.variable;

public class Exam02 {
	public static void main(String[] args) {
		
		int var1 = 0b1011;   //2진수
		int var2 = 0206;     //8진수
		int var3 = 365;      //10진수
		int var4 = 0xB3;     //16진수
		
		System.out.println(var1);
		System.out.println(var2);
		System.out.println(var3);
		System.out.println(var4);
		
		
		//byte -128 ~ 127
		byte bVall = -128;
		byte bVal2 = 0;
		byte bVal3 = 127;
		long bVal4 = 500;
		
		//long
		
		long lVal = 10;
		long lVal2 = 20L;
		long lVal3 = 100000000000L;   //L 붙여줘야 long으로 인식함
		
		//char (유니코드)
		//문자만 넣을수 있음. 문자 , 문자열은 다름
		char cVar = 'A';      //65
		char cVar2 = '가';
		char cVar3 = 67;      //C
		char cVar4 = 0x0041;  //A
		
		System.out.println(cVar);
		System.out.println(cVar2);
		System.out.println(cVar3);
		System.out.println(cVar4);
		
		//"" 문자열 String
		
		String str = "홍길동";
		//String str1 = '홍길동';  에러남. " " 사용
		String str2 = "프로그래머";
		System.out.println(str);
		System.out.println(str2);
		
		// 문자열 내부에 / 는 이스케이프를 뜻함
		// /n 줄바꿈 /t 탭 
		
		System.out.println("번호\t이름\t직업");
		//탭 만큼 띄움
		System.out.println("행 단위 출력\n");
		//엔터
		System.out.println("우리는\"개발자\"입니다.");
		//특수문자 존재 여부 확인 \"  \"  출력문에 " " 출력
		System.out.println("봄\\여름\\가을\\겨울");
		//특수문자 존재 여부 확인
		
		//실수 타입 float double
		float fVal = 3.14f;
		double dVal = 3.14;
		System.out.println(fVal);
		System.out.println(dVal);
		
		//e사용하기 제곱근 
		double dVal2 = 3e6;  //3 * 10의 6승
		float fVal2 = 3e6f;  //3 * 10의 6승
		double dVal3 = 2e-3; //2 * 10의 -3승  => 2/1000
		double temp11 = 50000000-10;
		System.out.println(temp11);  //4.999999E7
		System.out.println(dVal2);
		System.out.println(fVal2);
		System.out.println(dVal3);
		
		//논리타입 true false
		
		boolean stop = true;
		
		if(stop) {
			System.out.println("중지합니다.");
		}else {
			System.out.println("시작합니다.");
		}
		
		
		
		
		
		
	}
}
