package com.yedam.variable;

public class Exam01 {
	static int v4 = 10; //class 안 전체가 변수의 범위
	
	public static void main(String[] args) {
		int value;
		value = 10;
		System.out.println(value);
		
		int value2 = 20;
		int value3;
		//int result = value3 + 10; 에러남
		
		//변수 값 복사, 변수 3개 필요함
		int x = 3;
		int y = 5;
		int temp;    //임시 temp
		
		System.out.println("x :" + x + ", " + "y: "+ y);
		
		temp = x;
		x = y;
		y = temp;
		
		System.out.println("x :" + x + ", " + "y: "+ y);
		//중괄호 안에 있는 변수 = 로컬번수
		
		int v1 = 15;
		
		if(v1 > 10) {
			int v2;
			v2 = v1 -10;
		}
		int v3 = v1 +5;
		System.out.println(v3);
		System.out.println(v4);
		
		
	}
	
	//이 위치에서는 x,y 변수 사용 못함, 중괄호에서 나오면 변수가 메모리에서 삭제되어서
	//중괄호 밖 변수 = 전역변수
}
