package com.yedam.inter2;

public class Car {
	
		Tire frontLeftTire = new HankookTire();
		Tire frontRightTire = new KumhooTire();
		Tire backLeftTire = new HankookTire();
		Tire backRightTire =new KumhooTire();
		
		
	public void run() {
		frontLeftTire.roll();
		frontRightTire.roll();
		backLeftTire.roll();
		backRightTire.roll();
	}

}
