package com.yedam.inter2;

public class CarExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car myCar = new Car();
		
		
		myCar.run();
		
		System.out.println("===================");
		
		myCar.frontLeftTire = new KumhooTire();
		myCar.backRightTire = new HankookTire();

		myCar.run();
		
		
		
	}

}
