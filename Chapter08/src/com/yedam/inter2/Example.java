package com.yedam.inter2;

public class Example {
	public static void main(String[] args) {
		
		ImplementC imp1 = new ImplementC();
		
		InterfaceA ia = imp1;
		ia.methodA();
		//ia.methodB(); 부모가 자식을 바라볼수 없어서
		
		System.out.println();
		
		
		InterfaceB ib = imp1;
		ib.methodB();
		
		System.out.println();
		
		InterfaceC ic = imp1;
		ic.methodA();
		ic.methodB();
		ic.methodC();
		
	}
}
