package com.yedam.inter2;

public class Inheri {
	public static void main(String[] args) {
		
		//1)A인터페이스 <- B클래스 implements <- D클래스 extends
		//2)A <- D
		
		//A<-B
		A a = new B();
		a.info();
		
		//A <- D
		A a2 = new D();
		a2.info();     
		//D의 info 출력 -> 나는 D 입니다.
		//D에 info가 정의되어있지 않으면 부모를 바라봐서 B의 info를 사용
		
		
		
		
	}
}
