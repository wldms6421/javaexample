package com.yedam.inter2;

public class Application {
	
	public static void main(String[] args) {
		//인터페이스를 활용한 다형성
		//run이 재정의되어있는걸 실행
		Vehicle v1 = new Bus();
		drive(v1);
		//drive 는 뭐야. drive메소드의 매개변수가 되어버림.
		
		
//		Vehicle v2 = new Taxi();
//		drive(v2);
		
		v1.run();
		//v1.checkFare();  그냥 쓰면 에러. 강제타입변환해줘야 함.
		//부모클래스가 가지고 있는 메소드만 사용할수 있어서 Vehicle것만 사용가능한 상태
		//자식이 가지고 있는거 쓰고 싶으면 강제타입변환
		//캐스팅
		// Vehicle vehicle = new Taxi();
		//Bus bus = (Bus) vehicle;
		//객체 타입 확인 instance of
		
		/*
		Bus bus1 = new Bus();
		Taxi taxi = (Taxi) bus1;
		잘못된 예, Bus 줬는데 Taxi x;
		*/
		
		Bus bus = (Bus) v1;
		
		bus.run();
		bus.checkFare();
		
		
		//강제타입변환 확인
		drive(new Bus());
		drive(new Taxi());
	}
	
	
	
	
	//메소드
	public static void drive(Vehicle vehicle) {
		if(vehicle instanceof Bus) {
			Bus bus = (Bus) vehicle;
			bus.checkFare();
			//타입변환이 잘 되었는지 확인용
		}
//		else if(vehicle instanceof Taxi) {
//			Taxi taxi = (Taxi) vehicle;
//			taxi.run();
//		}
		
	
		vehicle.run();
		
	}
	
	
}
