package com.yedam.inter2;

public interface InterfaceC extends InterfaceA, InterfaceB{
	
	public void methodC();
	//보이지 않지만 methodA, methodB를 가지고 있는 것.
	//A기능+B기능+C기능
	
	
}
