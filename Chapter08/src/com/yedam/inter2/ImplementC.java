package com.yedam.inter2;

public class ImplementC implements InterfaceC{
	//A,B,C
	@Override
	public void methodA() {
		System.out.println("ImpleC - methodA실행");
		
	}

	@Override
	public void methodB() {
		System.out.println("ImpleC - methodB실행");
		
	}

	@Override
	public void methodC() {
		System.out.println("ImpleC - methodC실행");
		
	}
	
}
