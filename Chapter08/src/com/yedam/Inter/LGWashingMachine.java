package com.yedam.Inter;

public class LGWashingMachine implements WashingMachine{

	
	public void dry() {
		System.out.println("건조 코스 진행");
	}
	
	
	@Override
	public void startBtn() {
		// TODO Auto-generated method stub
		System.out.println("빨래 시작");
		
	}

	@Override
	public void pauseBtn() {
		// TODO Auto-generated method stub
		System.out.println("빨래 일시정지");
	}

	@Override
	public void stopBtn() {
		// TODO Auto-generated method stub
		System.out.println("빨래 중지");
	}

	@Override
	public int changeSpeed(int speed) {
		// TODO Auto-generated method stub
		int nowSpeed =0;
		switch(speed) {
		case 1:
			nowSpeed = 20;
			break;
		case 2:
			nowSpeed = 50;
			break;
		case 3:
			nowSpeed = 100;
			break;
		}
		return nowSpeed;
		
		
		
	}
	
	
}
