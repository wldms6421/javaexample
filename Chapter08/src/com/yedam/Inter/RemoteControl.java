package com.yedam.Inter;

public interface RemoteControl extends Searchable{
	//인터페이스끼리도 상속이 가능. 검색기능
	
	
	//리모컨의 기능만 정리.
	//인터페이스 , 상수 생성
	public static final int MAX_VOLUME = 10;
	public int MIN_VOLUME = 0;
	//	static final 생략 가능.
	
	
	//추상 메소드, abstract 생략 가능.
	public void turnOn();
	public abstract void turnOff();
	public void setVolume(int volume);
	//public void search(String string); 상속받았으니까 필요 없음.
	
	
}
