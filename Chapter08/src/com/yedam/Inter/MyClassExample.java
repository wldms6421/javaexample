package com.yedam.Inter;

public class MyClassExample {
public static void main(String[] args) {
	
	System.out.println("=====1)필드에 인터페이스===========");
	MyClass myClass = new MyClass();
	
	myClass.rc.turnOn();
	myClass.rc.turnOff();
	//RemoteControl rc = new Television();
	//Myclass에 rc는 다른지역에 또 있어서 그걸 또 가져오기 위해
	//rc.turnOn(); 사용. 이중으로 정보 가져오기.
	
	System.out.println("======2)생성자 매개변수에 인터페이스 사용===");
	MyClass myClass2 = new MyClass(new Audio());
	//매개변수로 자식클래스를 넘겨주게 됨. Audio의 turnOn/Off가 실행
	//이미 Audio에 turnOn/Off지정해주었기 때문에 밑 처럼 쓸 필요 없음.
//	myClass2.rc.turnOn();
//	myClass2.rc.turnOff();
	
	System.out.println("======3)메소드 안에서 로컬변수로 사용=======");
	MyClass myClass3 = new MyClass();
	myClass3.method1();
	
	
	System.out.println("=====4)메소드 매개변수로 사용==============");
	MyClass myClass4 = new MyClass();
	
	myClass4.methodB(new Television());
	
	
	
	
}
}
