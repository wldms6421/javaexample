package com.yedam.oop;

import com.yedam.access.Access;




public class Application20 {
	
	
		
	//일반 인스턴스와 run메소드
	int speed;
	//static int speed;
	void run() {
		System.out.println(speed + "으로 달립니다.");
	}
	
	
	
	//main문도 static 정적, 메소드 영역에 등록된 친구들
	public static void main(String[] args) {
		//int speed2 = speed; 에러남 speed 는 자기 영역 애가 아니라서.
		//int speed2 = speed;
		
		//run();  static형 아니라서 접근 불가
		
		Application20 app = new Application20();
		//내 자신의 클래스으 객체를 만들어서 static 안에서 형성시켜 주고 접근하면 가능.
		app.speed =5;
		app.run();
		
		
		
		
		Car myCar = new Car("포르쉐");
		Car yourCar = new Car("벤츠");
		
		myCar.run();
		yourCar.run();
		
		//정적 필드, 메소드 부르는 방법
		//정적 멤버가 속해있는 클래스명.필드 또는 메소드 명
		//1)정적 필드 가져오는 방법
		double piRatio =Calculator.pi;
		System.out.println(piRatio);
		//2) 정적 메소드 가져오는 방법
		int result = Calculator.plus(5, 6);
		System.out.println(result);
		
		//★★★★★★★★★★특징 1) 모든 클래스에서 사용할 수 있다. -> 공유의 기능
		//★★★★★★★★★★단점 2) 너무 남용해서 사용하는 경우 메모리 누수(부족) 현상이 발생할 수 있다.
		// ★ 주의할 점 ★
		//메소드 영역에 저장이 되기 때문에
		//static은 아무 곳에서나 사용하지만
		//static에 접근하는 것은 static으로 형성된 인스턴스나, 메서드만 가능하다. 일반 int a= ; 은 접근에러
		//static으로 된 애들은 같은 지역에 살고 있어서 접근이 가능.
		//객체를 만들어서도 사용할 수 있다.
		//정적 메소드에서 외부에 정의한 필드를 사용하려고 한다면, 
		//static이 붙은 필드 또는 메소드만 사용 가능.
		//static이 붙이지 않고 사용하고 싶다면,
		//해당 필드와 메소드가 속해 있는 클래스를 인스턴스화 하여서
		//인스턴스 필드와 인스턴스 메소드를 dot(.) 연산자를 통해 가져와서 사용.
		
		
		
		Person p1 = new Person("123123-123456", "김또치");
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
		
		// p1.nation = "USA"; final 값이라서 바꿀 수 없음
		
		//ConstnatNo.EARTH_ROUND;
		System.out.println(5*5*ConstnatNo.PI);
		
	}
}
