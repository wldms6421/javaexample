package com.yedam.oop;

public class Bookapp {
  public static void main(String[] args) {
	
	  Book book = new Book();
	  
	  // 생성자(매개변수) -> 기본 생성자를 하나 만들어줘야지 에러 안남..!
	  
	  book.kind = "학습서";
	  book.price = 24000;
	  book.publisher = "한빛미디어";
	  book.isbn = "yedam-001";
	  
	  
	  System.out.println("책이름 : 혼자 공부하는 자바");
	  book.getInfo();
	
  }	
}
