package com.yedam.oop;

import com.yedam.access.Access;

public class Aplication5 {
	public static void main(String[] args) {
		
		Access access = new Access();
		
		//public
		access.free = "free";
		
		//protected , 상속관계여야 or 같은 패키지거나, 에러
		access.parent = "parent"; 
		
		//private , 에러
		access.privecy = "privecy";  
		
		//default
		access.basic = "basic";
		
		access.free();
		access.privacy();
	}
}
