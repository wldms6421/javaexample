package com.yedam.oop;

public class Book {
//	String name;
//	String kind;
//	int price;
//	String press;
//	String isbn;
//	
//	
//	
//	 void getInfo() {	
//		System.out.println("종류: " + kind);
//		System.out.println("가격: " + price);
//		System.out.println("출판사: " + press);
//		System.out.println("도서번호: " + isbn);
//	}
	 
	String bookName;
	String kind = "학습서";
	int price;
	String publisher;
	String isbn;
	//생성자
	
	public Book() {
		
	}
	
	
	public Book(String bookName, int price, String publisher,String isbn) {
		this.bookName = bookName;
		this.price = price;
		this.publisher = publisher;
		this.isbn =isbn;
		
	}
	
	
	void getInfo() {
		System.out.println("종류: " + kind);
		System.out.println("가격: " + price);
		System.out.println("출판사: " + publisher);
		System.out.println("도서번호: " + isbn);
	}
	
	
	
}
