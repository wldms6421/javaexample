package com.yedam.oop;

public class SmartPhone {
    // 필드
	//객체의 정보를 저장하는 공간
	String name;  //하나의 종류만 쓸 경우에
	String maker;
	int price;
	

	
	
	//생성자(클래스 이름과 똑같이 부여해서 만듬)
	//public SmartPhone() {
	//자바에서 생성자가 클래스 내부에 "하나도 없을 때" 알아서 기본 생성자를 	
	// 만들고 객체를 생성.	
	//} 
	//매개변수 가진 생성자 - 오버로딩
	public SmartPhone() {
		
		this.name = "iphone14Pro";
	}
	
	public SmartPhone(String name) {
		//객체를 만들때 내가 원하는 행동 데이터 저장 등을 할때
		//내용을 구현하면 됨
		
		
		
	}
	
	public SmartPhone(int price) {
		//매개변수 int price 같은 타입과, 매개변수의 수에 따라 다 다르게 인식
		
	}
	
	public SmartPhone(String name, int price) {
		this.name = name;   
		this.price = price;
		//필드에 있는 데이터를 초기화하면서 사용하기 위한 방법
		//this.name은 클래스의 필드에 있는 name 
	}
	
	public SmartPhone(String name, String maker, int price) {
		
		
	}
	
	
	
	//메소드
	//객체의 기능을 정의

	void call() {
		System.out.println(name + "전화를 겁니다.");
		
	}
	void hangUp() {
		System.out.println(name + "전화를 끊습니다.");
	}
	
	//1) 리턴타입이 없는 경우  :void    { 실행문 } 만 실행하고 끝냄
	 
	void getInfo(int no) {
		System.out.println("매개 변수 : " + no);			
	}
	
	
	//2) 리턴타입이 있는 경우 
	//1. 기본타입 : int, double, long..
	//2. 참조타입 : String, 배열, 클래스..
	
	//2-1) 리턴타입이 기본타입인 경우
	int getInfo(String temp) {
		
		return 0; //꼭 값을 돌려줘야 함 int 타입이니까 int타입으로 return
	}
	
	
	//2-2) 리턴타입이 참조 타입인 경우
	String[] getInfo(String[] temp) {
		
		
		return temp;  //날 부른 애한테 리턴 temp와 string[] 타입이 같게
	}
	
	
	
	
	
	
	
	
}
