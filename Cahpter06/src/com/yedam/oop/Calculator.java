package com.yedam.oop;

public class Calculator {
	//필드
	//정적 필드  
	static double pi = 3.14;  
	
	
	//생성자
	
	
	
	//메소드
	//정적 메소드
	static int plus(int x, int y) {
		return x+y;
	}
	
	
	
	//메소드   , 오버로딩 기준
	int sum(int a, int b) {
		result("메소드 연습");
		return a+b;
	}
	
	
	
	
	//메소드 오버로딩 , 데이터 타입 차이에 따른 오버로딩
	double sum(double a, double b) {
		return a+b;
	}
	
	//변수 개수에 따른 오버로딩  매개변수가 2->1개로 변했음.
	int sum(int a) {
		return a;
	}
	
	
	//매개변수의 데이터타입에 따른 오버로딩  int,int -> double,int
	double sum(double a, int b) {
		return a+b;
	}
	
	
	//매개변수의 순서 차이에 따른 오버로딩 double,int -> int double
	double sum(int a, double b) {
		return a+b;
	}
	
	
	
	
	
	
	double sub(int a, int b) {
		return a-b;
	}
	
//	String result(String value) {
//		String temp = "value return 테스트 : " + value;
//		return temp;
//	}
	
	
	void result(String value) {
		System.out.println("value return 테스트 : " + value);
	}
//	반환하지 않고 바로 출력
	
	
//	
//	void lotto() {
//		
//		//로또 번호 출력
//		//로또 번호 생성
//		//로또 번호 생성 횟수 제한
//		//자주쓰는 기능들을 메소드로 정의해서 쓸 때 불러와서 쓸 수 있도록 구성
//	
//	}
//	
//	void createNum() {
//		
//		
//	}
//	//로또번호생성 메소드 생성
	
	
	
	
	
	
	
	
	
	
	
	
	
}
