package com.yedam.oop;

public class Application2 {
	public static void main(String[] args) {
		/*
		Calculator cl = new Calculator();
		
		int sumResult = cl.sum(1, 2);
		
		double subResult = cl.sub(10, 20);
		
		
		System.out.println(sumResult);
		System.out.println(subResult);
		System.out.println(cl.sum(4, 4));
		//System.out.println(cl.result("메소드 연습"));  //리턴값 있음
		cl.result("메소드 연습");  //리턴값이 없는 것 실행
		*/
		
		
		Computer myCom = new Computer();
		int result = myCom.sum(1,2,3);
		System.out.println(result);
		System.out.println();  //엔터키
		result = myCom.sum(1,2,3,4,5,6);
		System.out.println(result);
		
		
		
		
	}
}
