package com.yedam.oop;

import com.yedam.access.Access;

public class Aplication {
	public static void main(String[] args) {
		Access access = new Access();
		
		//public
		access.free = "free";
				
		//protected , 상속관계여야 or 같은 패키지거나
		access.parent = "parent"; 
				
		//private
		access.privecy = "privecy";  
				
		//default
		access.basic = "basic";
		
	}
}
