package com.yedam.oop;

public class Application {
  public static void main(String[] args) {
	  //SmartPhone  클래스(설계도)를 토대로 iphone 구현
	  SmartPhone iphone14Pro = new SmartPhone("Apple", "iphone14Pro", 500);
	  //정보를 넣는 방법
//	  iphone14Pro.maker = "Apple";  
//	  iphone14Pro.price = 100000;
//	  iphone14Pro.name = "iphone14Pro";
	  iphone14Pro.call();
	  iphone14Pro.hangUp();
	  
	  
	  //필드 정보 읽기
	  System.out.println(iphone14Pro.maker);
	  System.out.println(iphone14Pro.name);
	  System.out.println(iphone14Pro.price);
	  
	  
	  //스마트폰 기반으로 또 다른 물건 만들기
	  //SmartPhone 클래스(설계도)  - 여러물건을 만들기 위해 설계도를 구성한 것
	  //클래스의 재사용성
	  
	  SmartPhone zfilp4 = new SmartPhone("samsung","zfilp4",10000);
	  
	  zfilp4.maker = "samsung";
	  zfilp4.name = "zfilp4";
	  zfilp4.price = 10000;
	  
	  zfilp4.call();
	  zfilp4.hangUp();
	  //같은 설계도를 썼는데 초기화한 값들에 따라 다른 결과물이 나옴
	  
	  //System.out.println(iphone14Pro.maker);
	  
	  
	  
	  
	  
	  SmartPhone sony = new SmartPhone();
	  
	  //리턴타입이 없는 메소드
	  //int a = sony.getInfo(0);  실행만하고 와서 리턴된 값이 없음..에러
	  sony.getInfo(0);    //기능만 실행할 경우 사용하면 좋음
	  
	  //리턴타입이 int인 메소드
	  int b = sony.getInfo("int");  //결과값은 0
	  
	  
	  //리턴타입이 String[]인 메소드
	  //args[] ? 
	  
	  
	  String[] temp = sony.getInfo(args);
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
  }
}
