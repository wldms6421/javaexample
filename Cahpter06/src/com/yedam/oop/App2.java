package com.yedam.oop;

public class App2 {
  public static void main(String[] args) {
	
	  Student about = new Student();
	  
	  about.stdName = "고길동";
	  about.stdSchool = "예담고등학교";
	  about.stdNo = "221124";
	  about.eng = 70;
	  about.kor = 70;
	  about.math = 70;
	  
	  about.getInfo();
	  
	  int a = about.sum();
	  double b = about.avg();
	  
	  System.out.println(a);
	  System.out.println(b);

	  
	  //Student about1 = new Student("김둘리", "예담고등학교", 221125, sum(50,50,50), avg(50,50,50));
	
	  //about1.sum(50,50,50);
	  
  }
}
