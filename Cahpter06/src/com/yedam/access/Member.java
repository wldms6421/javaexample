package com.yedam.access;

public class Member {

	//필드
	private String id;
	private String pw;
	private String name;
	private int age;
	//음수라는 데이터는 담지 않도록
	
	
	//생성자
	
	
	
	
	
	//메소드
	//setter, getter 데이터의 무결성을 지키기 위해 사용
	
	public void setAge(int age) {
		if(age<0) {  //말도 안되는 데이터가 들어오면
		//매개변수로 음수의 값이 들어오면, age는 음수가 됨
		//무결성을 지켜주기 위해서 조건문 사용.
		System.out.println("잘못된 나이입니다.");
		//this.age = 0; //음수가 들어오면 다 0으로 처리하겠다는 방법을 사용해도 됨
		return;      //return -> return을 만나면 하던것을 멈추고 메서드를 종료한 후 
		             //메소드 호출한 곳으로 이동. 보통 void에서 return 쓰지 않음.
		
		}else {
			this.age =age;
		}
		System.out.println("return문을 만나지 않았습니다.");
	
		
		
	}
	
	
	public int getAge() {
		//미국의 나이와 한국의 나이는 한살 차이 나므로
		//아래 내용을 실행한다.
		age = age +1;
		return age;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		if(id.length()<=8) {
			System.out.println("8글자보다 부족합니다. 다시 입력해주세요");
			return;
		}
		this.id = id;
	}


	public String getPw() {
		return pw;
	}


	public void setPw(String pw) {
		this.pw = pw;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
