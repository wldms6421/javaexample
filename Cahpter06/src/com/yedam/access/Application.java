package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Access access = new Access();
		
		//public
		access.free = "free";
				
		//protected , 상속관계여야 or 같은 패키지거나
		access.parent = "parent"; 
				
		//private
		//access.privecy = "privecy";  
				
		//default
		access.basic = "basic";
	
			Singleton obj1 = Singleton.getInstance();
			Singleton obj2 = Singleton.getInstance();
		
				if(obj1 == obj2) {
					System.out.println("같은 싱글톤 객체이다.");
				}else {
					System.out.println("다른 싱글통 입니다.");
				}
	}
}
