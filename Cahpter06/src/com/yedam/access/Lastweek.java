package com.yedam.access;

import java.util.Scanner;

public class Lastweek {

	public static void main(String[] args) {
		//다음을 만족하는 Student 클래스 작성
		//이름, 학과, 학년, 과목별 점수
		// programing, DataBase, Os
		//필드들은 모두 private 설정
		//setter통해 필드 초기화
		//getter 통해 데이터 reading
		//getter 통한 학생의 정보를 출력
		//출력 예시  학생의 이름: 김또치
		//학과 : 컴퓨터공학과
		//학년 : 2학년  프로그래밍 언어 점수 :80
		//데이터베이스 점수 :70    OS 점수 : 90
		//Scanner sc = new Scanner(System.in);
		
		Student std1 = new Student();
		std1.setStdName("김또치");
		std1.setStdGrade("2학년");
		std1.setMajor("컴퓨터공학과");
		std1.setPrograming(50);
		std1.setDataBase(60);
		std1.setOS(90);
		//데이터입력은 set
		
		System.out.println("이름 : " + std1.getStdName());
		//가져오는건 get
		System.out.println("학과 : " + std1.getMajor());
		System.out.println("학년 : " + std1.getStdGrade());
		System.out.println("프로그래밍 언어 점수 : " + std1.getPrograming());
		System.out.println("데이터베이스 점수 : "+ std1.getDataBase());
		System.out.println("운영체제 점수 : "+ std1.getOS());
		
		
	}
}
