package com.yedam.access;

public class Access {
	/*
	 * public 어디서든 누구나 다 접근 가능
	 * protected 상속 받은 상태에서 부모- 자식간에 사용 (패키지가 달라도 사용 가능)
	 * 				패키지 다른 사용 못함, 같은 패키지에서만 사용 가능
	 * default 패키지 다른 사용 못함, 같은 패키지에서만 사용 가능
	 * private 내가 속한 클래스에서만 사용 가능
	 */
	//접근제한자 -> 이름을 지어서 사용(변수, 클래스, 메소드 등등) 다 됨.
	//필드
	public String free;
	protected String parent;
	private String privacy;    //Access에서만 사용할 수 있음.
	String basic;   //default타입
	
	
	
	
	//생성자
	public Access() {
		
	}
	
	
	private Access(String privacy) {
		this.privacy = privacy;
	}
	
	
	//메소드
//	public void run() {
//		System.out.println("달립니다.");
//	}
	
	public void free() {
		System.out.println("접근이 가능합니다.");
		privacy(); // free 안에 privacy 를 넣어주면 접근 가능하게 됨. 내부에서 
		//회사의 특허기술을 사용가능하도록. 비밀은 지키고.
	}
	
	
	//클래스 내부에서 각자 다른 메서드를 호출해서 사용하기도 하는데, 그 때 정보를 숨기기 위해서 .
	private void privacy() {
		System.out.println("접근이 불가능합니다.");
	}
}
