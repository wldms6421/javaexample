package com.yedam.access;

import java.util.Scanner;

public class Student {
//	Scanner sc = new Scanner(System.in);
//	private String name;
//	private String major;
//	private int year;
//	private int score;
//	private String programing;
//	private String DataBase;
//	private String Os;
//	
//	public String getPrograming() {
//		return programing;
//	}
//	public void setPrograming(String programing) {
//		this.programing = programing;
//	}
//	
//	
//	public String getDataBase() {
//		return DataBase;
//	}
//	public void setDataBase(String dataBase) {
//		DataBase = dataBase;
//	}
//	
//	
//	public String getOs() {
//		return Os;
//	}
//	public void setOs(String os) {
//		Os = os;
//	}
//	
//	
//	public String getName() {
//		
//		System.out.println("학생 이름을 입력해주세요. ");
//		name = sc.nextLine();
//		System.out.println("학생이름 : " + name);
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//		
//	}
//	
//	
//	public String getMajor() {
//		
//		System.out.println("학생의 학과를 입력해주세요. ");
//		major = sc.nextLine();
//		System.out.println("학과 : "   + major);
//		return major;
//	}
//	
//	public void setMajor(String major) {
//		this.major = major;
//	}
//	
//	
//	public int getYear() {
//		System.out.println("학생의 학년를 입력해주세요. ");
//		year = Integer.parseInt(sc.nextLine());
//		System.out.println("학년 : "   + year);
//		return year;
//	}
//	public void setYear(int year) {
//		this.year = year;
//	}
//	
//	
//	public int getScore() {
//		System.out.println("학생의 점수를 입력해주세요. ");
//		score = Integer.parseInt(sc.nextLine());
//		System.out.println("학년 : "   + score);
//		return score;
//	}
//	public void setScore(int score) {
//		this.score = score;
//	}
	
	//필드
	private String stdName;
	private String major;
	private String stdGrade;
	private int programing;
	private int dataBase;
	private int OS;
	
	
	//생성자
	//클래스를 통한 객체를 생성할 때 첫번째로 수행하는 일들을 모아두는 곳.
	//필드에 대한 데이터를 객체를 생성할 때 초기화 할 예정이라면
	//생성자에서 this키워드를 활용해서 필드 초기화하면 됨.
	
	
	
	
	
	//메소드
	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		
		this.stdName = stdName;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		
		this.major = major;
	}
	public String getStdGrade() {
		return stdGrade;
	}
	public void setStdGrade(String stdGrade) {
		
		this.stdGrade = stdGrade;
	}
	public int getPrograming() {
		return programing;
	}
	
	//0보다 작은 점수 드러오는 정우 프로그래밍 언어 점수 0으로 처리.
	public void setPrograming(int programing) {
		if(programing<=0) {
			this.programing = 0;
		}
		this.programing = programing;
	}
	public int getDataBase() {
		return dataBase;
	}
	public void setDataBase(int dataBase) {
		if(dataBase<=0) {
			this.dataBase = 0;
		}
		this.dataBase = dataBase;
	}
	public int getOS() {
		return OS;
	}
	public void setOS(int oS) {
		if(OS<=0) {
			this.OS = 0;
		}
		this.OS = oS;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
