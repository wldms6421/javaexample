package com.yedam.abs;

public class AnimalExample {
 public static void main(String[] args) {
	
	 Cat cat = new Cat();
	 cat.sound();     //야옹
	 
	 System.out.println();
	 
	 //Animal은 추상클래스라 직접적으로 사용할수 없어서 자식 클래스인 Cat 활용
	 Animal animal = new Cat();
	 animal.sound();   //야옹
	 animal.breathe();  //숨을 쉽니다.
	 System.out.println(animal.kind);  //포유류
	 
	 animalSound(new Cat());
	 
	 //추상클래스 -> 클래스와 별반 다른 게 없다.
	 //차이점 1) 자기 자신을 객체로 만들기 못한다.
	 //     2) 추상 메소드가 존재하며, 상속을 받게 되면 추상 메소드는 반드시 반드시 구현해야 한다.
	 //     3) 스스로 객체(인스턴스)화가 안되므로 자식클래스를 통한 자동타입변환으로 구현
}
 
 
 //매개변수를 활용한 자동 타입변환
 //static 맞춰주기
 	public static void animalSound(Animal animal) {
 		animal.sound();
 		
 	}
 
}
