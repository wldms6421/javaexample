package com.yedam.abs;

public class Cat extends Animal{

	
	
	public Cat() {
		this.kind = "포유류";
	}
	
	@Override
	public void sound() {
		// 추상 클래스 안의 추상 메소드를 쓰기 위해 강제 오버라이딩.
		System.out.println("야옹");
	}
	
	
	
}
