package com.yedam.abs;

public class PhoneExample {

	//추상 클래스 객체(인스턴스)화 확인
	//Phone phone = new Phone("주인");
	//자기자신은 객체로 만들기 못해서 에러.
	//메소드에 추상 abstract 의 키워드가 있다면 그 자식 클래스는 반드시 오버라이딩해줘야 
	public static void main(String[] args) {
		
		SmartPhone smartPhone = new SmartPhone("홍길동");
		
		smartPhone.turnOn();
		smartPhone.turnOff();
		
		smartPhone.internetSearch();
		
	
	
	
	}
}
