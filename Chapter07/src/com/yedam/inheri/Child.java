package com.yedam.inheri;

public class Child extends Parent {
	//자식 클래스
	//public String firstName;
	public String lastName;
	public int age;
	//선택해서 가져와 쓸 수 있음.
	
	
	// 메소드 오버라이딩 예제
	//오버라이딩 했다고 알려주는 방법
	@Override
	//private를 사용할수는 없음. 부모가 protected로 정의되어있음.
	public void method1() {
		System.out.println("child class -> method1 Override내용바꾼");
	}
	
	public void method3() {
		System.out.println("child class -> method3 Call");
	}

	
	
	
	
	
	
}
