package com.yedam.inheri;

public class AirPlaneExample {
	public static void main(String[] args) {
	
		SuperSonicAirPlane sa = new SuperSonicAirPlane();
		
		sa.takeOff();
		sa.fly();  //일반
		
		sa.flyMode = SuperSonicAirPlane.SUPERSONIC;
		
		sa.fly();  //초음속
		
		sa.flyMode = SuperSonicAirPlane.NORMAR;
		
		sa.fly();
		
		sa.land();
		
		
	}
}
