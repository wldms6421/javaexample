package com.yedam.inheri;

public class OverrideExam {
public static void main(String[] args) {
	
	Child child = new Child();
	
	child.method1();   //자식이 부모의 메소드를 똑같이 써서 바꾼 정보를 출력
	child.method2();   //부모가 가진 메소드 호출
	child.method3();   //부모에게 없어서 자식이 가진 메소드가 호출됨
	
}
}
