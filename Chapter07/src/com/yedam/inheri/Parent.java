package com.yedam.inheri;

public class Parent {
	//부모클래스
	//private는 class내부에 있는 애라서 접근제한- 상속대상 제외
	//부모패키지가 다른 패키지에 존재하는 경우 default - x
	//protected인 경우 -o
	//1)상속할 필드 정의
	//다른 패키지에서 사용하기 위해서 public이나  protected
	 protected String firstName = "Lee";
	 protected String lastName;
	 protected String DNA = "B";
	
	//2) 상속 대상에서 제외
	private char bloodType = 'B';
	public int age;
	
	
	//메소드
	protected void method1() {
		System.out.println("parent class -> method1 Call");
	}
	public void method2() {
		System.out.println("parent class -> method2 Call");
	}
	
	
	
}
