package com.yedam.inheri;

public class Application {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.lastName =  "또치";
		child.age = 20;
		
		System.out.println("내 이름 : " + child.firstName + child.lastName);
		System.out.println("DNA : " + child.DNA);
		//private로 설정되어서 child클래스에서는 부모클래스의 bloodType 사용불가.
		//System.out.println("혈액형 : " + child.bloodType);
		System.out.println("나이 : " + child.age);
		//공통적인건 가져와서 쓰고 새로 지정해야하는건 지정해서 사용
		
		Child2 child2 = new Child2();
		
		child2.lastName = "희동";
		child2.age = 5;
		//child2.bloodType = 'A';
		
		System.out.println("내 이름 : " + child2.firstName + child2.lastName);
		System.out.println("DNA : " + child2.DNA);
		//child2에 있는 bloodType은 public이어서 사용가능.
		System.out.println("혈액형 : " + child2.bloodType);
		System.out.println("나이 : " + child2.age);
	}
}
