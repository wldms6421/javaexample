package com.yedam.inheri;

public class Person extends People{
	//people에 생성자가 있어서 자식은 부모가 가지고 있는 생성자를 따라야 에러 안남
	
	public int age;
	//자식 객체 만들때 , 생성자를 통해서 만든다.
	//super() 키워드를 통해서 부모 객체를 생성한다.
	//여기서 super가 의미하는 것은 부모 생성자를 호출
	//따라서 자식 객체를 만들게 되면 부모 객체도 같이 만들어진다.
	
	
	public Person(String name, String ssn) {
		super(name, ssn);
		//부모클래스에 만들어진 생성자를 super 사용해서 불러와야 에러 안나
	}
	
}
