package com.yedam.poly;

public class DrawExample {

	public static void main(String[] args) {
		//자동 타입 변환
		//부모타입 변수 = new 자식 클래스();
		
		Draw circle = new Circle();
		//Draw rectangle = new Rectangle();
		
		circle.x = 1;
		circle.y =2;
		
		circle.draw();
		
		circle =  new Rectangle();    //rectangle로 다시 초기화 시키면 rec내용을 참조
		
		circle.draw();
		
	}

}
