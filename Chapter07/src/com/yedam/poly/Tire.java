package com.yedam.poly;

public class Tire {
	//타이어 수명이 다되면 자식 클래스의 타이어로 갈아끼우겠다는 예제
	//차마다 타이어 규격이 다르니까 필드에 갈아끼움
	//필드
	public int maxRotation;  //최대 회전수 타이어수명
	public int accRotation;  //누적된 타이어 회전수
	public String location; //타이어 위치
	
	//생성자
	public Tire(String location, int maxRotation) {
		this.location = location;
		this.maxRotation = maxRotation;
	}
	
	
	
	//메소드
	//타이어 굴러가는 메소드
	public boolean roll() {
		++accRotation;
		if(accRotation < maxRotation) {
			System.out.println(location + "타이어의 수명 :" + 
					(maxRotation -accRotation) + "회");
			return true;
		}else {
			System.out.println("#######" + location + " Tire 펑크" + "######");
			return false;
		}
	}
	
	
	
	
	
	
	
}
