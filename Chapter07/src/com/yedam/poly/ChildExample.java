package com.yedam.poly;

public class ChildExample {

	public static void main(String[] args) {
		//1)
//		Child child = new Child();
//		Parent parent = child;
		//클래스간의 자동 타입 변환
		//부모클래스에 있는 메소드를 사용하되
		//자식 클래스에 재정의가 되어 있으면 그 메소드를 사용하겠습니다.
		
		
//		parent.method1();
//		parent.method2();
		//parent.method3(); //부모는 가지고 있지 않아서 에러
		
		//클래스간의 강제 타입 변환
		//자동 타입변환으로 인해서 다시 클래스 내부에 저장된 필드,메소드를 못 쓸 경우
		//강제 타입 변환을 함으로써 자식 클래스 내부에 정의된 것들을 사용할 수 있게.
		
		Parent parent = new Child();
		
		parent.field = "data1";
		parent.method1();
		parent.method2();
		
		Child child = (Child)parent;
		
//		parent.field2 = "data2";
//		parent.method3();   에러
		
		child.field2 = "data2";
		child.method3();
		child.method1();
		child.method2();
		
		
		//클래스타입 확인 예제
		
		method1(new Parent());
		method1(new Child());
		
		
		GrandParent gp = new Child();
		gp.method4();    //나는 손자 출력
		
		
	}
	
	
	//parent 자기와 타입변환된 애들이 들어올 수 있음.
	//자통타입변환 된 애들은 child타입으로 들어오게 됨.
	//자통타입변환애들만들어갈 수 있음.
	public static void method1(Parent parent) {
		//클래스타입 확인 예제
		
		if(parent instanceof Child) {
			Child child = (Child) parent;
			System.out.println("변환 성공");
		}else {
			System.out.println("변환 실패 ");
		}
		
		
	}
	
	
	
	
	
	

}
