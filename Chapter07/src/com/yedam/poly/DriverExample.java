package com.yedam.poly;

public class DriverExample {
public static void main(String[] args) {
	
	//부모 vehicle => Bus, Taxi 자식
	//Driver -> Vehicle 매게 변수로 하는 drive 존재
	//drive(Vehicle vehicle) <- 매개 변수에 자식 클래스를 대입
	
	Vehicle vehicle = new Vehicle();
	
	Driver driver = new Driver();
	//driver.drive(new Bus());
	//자동타입변환..  
	Bus bus = new Bus();
	driver.drive(bus);
	
	driver.drive(new Taxi());
}
}
