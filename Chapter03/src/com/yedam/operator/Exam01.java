package com.yedam.operator;

public class Exam01 {
	public static void main(String[] args) {
		//부호 연산자, + -
		int x = -100;
		int result1 = -x; //100
		int result2 = +x; //-100
		System.out.println("result1: "+ result1);
		System.out.println("result2: "+ result2);
		
		byte b = 100;
		//byte result3 = -b; 에러남
		int result3 = -b; //부호 연산자를 쓰면 int형으로 변환되서
		System.out.println("result3: "+ result3); //-100
		
		//증감 연산자 ++ --
		//위치에 따라서 연산 타이밍이 다름.
		
		int value = 100;
		
		System.out.println(value++);  //100 실행 후 증가
		System.out.println(++value);  //102 증가 후 실행
		
		//논리 부정 연산자 not !
		//true + not = false, false + not = true
		boolean flag = false;  //flag 기준점이라는 뜻
		if(!flag) {
			System.out.println("false");
		}
		if(flag) {
			System.out.println("true");
		}
		
		//이항 연산자 (사칙연산)
		int v1 = 10;
		int v2 = 4;
		System.out.println(v1+v2);
		System.out.println(v1-v2);
		System.out.println(v1*v2);
		
		//나누기가 2개가 있음
		v1 = 11;
		v2 = 4;
		System.out.println("/:"+ (v1/v2));
		System.out.println("% : " + (double)(v1%v2));
		
		//문자열 결합 연산자(+), 딱풀연산자
		System.out.println("자바" + "jdk" + "11버전");
		System.out.println("자바 jdk 11버전");
		
		
		//비교연산자
		int num1 = 10;
		int num2 = 10;
		
		System.out.println(num1 == num2);
		System.out.println(num1 != num2);   //false
		System.out.println(num1 >= num2);
		
		char char1 = 'A';
		char char2 = 'B';
		System.out.println(char1 < char2);
		System.out.println(char1 == char2); //false
		
		//문자열 비교는 equals 사용
		
		//논리 연산자 => && <->&  ||<->|
		//&& => 단 하나의 조건이 F이면 F (F && T && F && T)  => F
		//앞에 0이 나오면 뒤에 어떤 값이 나오든지 F로 인식
		//&의 경우에는 하나하나 다 따져보고 F 인식 네가지 값을 다 곱해봄
		
		// || => 단 하나의 조건 T(F || T || F || T)
		//하나의 명제가 T이면 전체를 true로 인식
		// | => 뒤에까지 다 계산.
		
		int charCode = 'A';
		//A~Z
		if(charCode>=65 && charCode<=90) {
			System.out.println("대문자.");
		}
		
		//48~57
		//charCode >= 48 charCode<=57
		//!<  =>  >=   !>  ->  <=   미만의 반대 이상, 
		if(!(charCode<48) && !(charCode>57)) {
			System.out.println("0~9숫자.");
		}
		
		int value2 = 6;    // 2또는 3의 배수
		if(value2%2==0 || value2%3==0) {
			System.out.println("2 또는 3의 배수");
		}
		
		//대입 연산자 =, +=, -=....
		int result5 = 0;
		result5 += 1; 
		
		result5 -= 1;
		result5 *= 1;
		result5 /= 1;
		
		//삼항 연산자 
		int score = 85;  //B
		score = 91;      //A
		char grade = (score>90) ? 'A' : 'B';
		System.out.println(grade);
		
		int grade2;
		if(score>90) {
			grade2 = 'A';
		}else {
			grade2 = 'B';
		}
		
	}
}
