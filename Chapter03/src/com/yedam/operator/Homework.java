package com.yedam.operator;

import java.util.Scanner;

public class Homework {

	public static void main(String[] args) {
		//문제1
		int x = 37;
		int y = 91;
			
		//문제2
		System.out.println("변수명1: " + x + " 변수명2: " + y);
		System.out.println(y+x);
		System.out.println(y-x);
		System.out.println(y*x);
		System.out.println(y/x);
		
		//문제3
		int var1 = 128;
		char var2 = 'B';
		char var3 = 44032;
		long var4 = 100000000000L;
		float var5 = 43.93106f;
		float var6 = 301.3f;
		
		
		System.out.printf("int: %d%n", var1);
		System.out.printf("char: %c%n", var2 );
		System.out.printf("char: %c%n", var3 );
		System.out.printf("long: %d%n", var4 );
		System.out.printf("float: %f%n", var5 );
		System.out.printf("float: %f%n", var6 );
		
		//문제4
		byte a = 35;
		byte b = 25;
		int c = 463;
		long d = 1000L;
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		
		long result = a + c + d;
		System.out.println(result);
		
		int result2 = a + b + c;
		System.out.println(result2);
		
		double e = 45.31;
		double result3 = c + d + e;
		System.out.println(result3);
		
		
		//문제5
		int intValue1 = 24;
		int intValue2 = 3;
		int intValue3 = 8;
		int intValue4 = 10;
		char charValue = 'A';
		String strValue = "번지";
		System.out.println(""+charValue+""+(intValue1+intValue2)+""+intValue3+""+strValue+""+(float)intValue4);

		System.out.println( String.valueOf(charValue) +( intValue1 + intValue2) + 
		intValue3 + strValue + (double)intValue4);
		
		
		//추가문제
		Scanner sc = new Scanner(System.in);
		System.out.println("숫자 입력>");
		String u = sc.nextLine();
		int innum = Integer.parseInt(u);
		
		int f = innum/100;
		System.out.println(f);
		int s = innum%100/10;
		System.out.println(s);
		int l = innum%100%10;
		System.out.println(l);
		int sum = f + s + l ;
		
		System.out.printf("각 자리수의 합: %d%n", sum);
		
		
		
		Scanner sc2 = new Scanner(System.in);
		System.out.println("숫자 입력>");
		String i = sc2.nextLine();
		int numm = Integer.parseInt(i);
		//풀이
		int hundred = numm / 100;                //4
		int ten = (numm - (100*hundred)) /10;  //8
		int one = (numm - (100*hundred) - (10*ten));   //5
		
		int intResult = hundred + ten + one;
		System.out.println(intResult);
		
		
		
		
	}
}
