package com.yedam.test;

import java.util.Scanner;

public class test1125result {
public static void main(String[] args) {
	
	/*
	 * 1. 주사위 크기 2.굴리기 3. 결과 4. 가장 많이 나온 수 5.종료
	 */
	
	Scanner sc = new Scanner(System.in);
	int[] dice = null;
	int size =0;
	
	boolean run = true;
	
	while(run) {
		System.out.println("1. 주사위 크기 2.굴리기 3. 결과 4. 가장 많이 나온 수 5.종료");
		System.out.println("메뉴>");
		String selectNo = sc.nextLine();
		
		switch(selectNo) {
		case "1":
			System.out.println("주사위 크기>");
			size = Integer.parseInt(sc.nextLine());
			//if 
			if(size <5 || size > 10) {
				System.out.println("입력한 값의 범위를 벗어났습니다.");
				System.out.println("5~10사이의 수를 입력해주세요.");
			}
			
			break;
		case "2":
			dice = new int[size];
			int count =0;
			while(true) {
				//5가 나올때까지 반복
				int random = (int)(Math.random()*size)+1;
				//각 숫자 나온 횟수 저장
				dice[random-1] = dice[random-1] +1;
				count++;
				if(random == 5) {
					break;
				}				
			}
			System.out.println("5가 나올때 까지 주사위를" + count + "번 굴렸습니다.");
			break;
		case "3":
			for(int i=0; i<dice.length; i++) {
				System.out.println((i+1) + "은" + dice[i] + "번 나왔습니다.");
			}
			break;
		case "4":
			//가장많이 나온 수
			int max = 0;
			int index =0;
			for(int num : dice) {
				if(max<num) {
					max = num;
				}
			}
			
			
			for(int i=0; i<dice.length; i++) {
				if(max<=dice[i]) {
					index = i;
				}
			}
			System.out.println("가장 많이 나온 수는" + max + "입니다.");
			break;
			
		case "5":
			System.out.println("프로그램을 종료하겠습니다.");
			break;
		
		
		
		}//switch
		
		
		
		
	}//while
	
	
	
	
	}
}
