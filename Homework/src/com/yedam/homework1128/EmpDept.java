package com.yedam.homework1128;

public class EmpDept extends Employee{
	

	//필드
	public String deptName;  //부서이름

	
			
	//생성자
	
	public EmpDept(String name, String salary, String deptName) {
		super(name, salary);    //부모클래스 객체 생성
		this.deptName = deptName;
		// TODO Auto-generated constructor stub
	}


	
	//메소드
	
	public String getDeptName() {
		return deptName;
	}



	@Override
	public void getInformation() {
		
		super.getInformation();
		System.out.println(" 부서 :" + deptName);
	}



	//슈퍼클래스 출력 + 서브클래스
	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}

		
}
