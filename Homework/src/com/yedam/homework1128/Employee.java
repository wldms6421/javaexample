package com.yedam.homework1128;

public class Employee {
		
		protected String name;
		protected String salary;
		
		//생성자
		public Employee(String name, String salary) {
			this.name = name;
			this.salary = salary;
		}


		//메소드
		

		public String getName() {
			return name;
		}

		public String getSalary() {
			return salary;
		}
		
		public void getInformation() {
			System.out.print("이름 :" + name + " 연봉 : " + salary);
		}
		
		
		public void print() {
			System.out.println("슈퍼클래스");
		}
		
		
}
