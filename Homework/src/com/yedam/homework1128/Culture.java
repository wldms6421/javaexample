package com.yedam.homework1128;

public abstract class Culture {
		//제목, 감독참여수, 참여배우수, 관객수, 총점
		public String title;
		public int director;
		public int actor;
		public int audience=0;
		public int total=0;
		
		
		
		//제목, 감독참여수, 참여배우수 생성자초기화
		public Culture(String title, int director, int actor) {
			this.title = title;
			this.director = director;
			this.actor = actor;
		}
		
		
		//관객수와 총점을 누적시키는 기능
		public void setTotalScore(int score) {
			this.audience++;  //누적
			this.total+= score;
			System.out.println("누적 관객수는 : " + audience + "누적 총점은 : " + total);
			
		}
		
		
		//평점을 구하는 기능
		public String getGrade() {
			int avg = total / audience;
			
			String grade = "";
			
			switch(avg) {
			case 1:
				grade = "☆";
				break;
			case 2:
				grade = "☆☆";	
				break;
			case 3:
				grade = "☆☆☆";	
				break;
			case 4:
				grade = "☆☆☆☆";
				break;
			case 5:
				grade = "☆☆☆☆☆";
				break;
			case 6:
				grade = "☆☆☆☆☆☆";
				break;
			case 7:
				grade = "☆☆☆☆☆☆☆";
				break;
			case 8:
				grade = "☆☆☆☆☆☆☆☆";
				break;
			case 9:
				grade = "☆☆☆☆☆☆☆☆☆";
				break;
			case 10:
				grade = "☆☆☆☆☆☆☆☆☆☆";
				break;
			}
			
			return grade;
			//2
//			for(int i = 0; i<avg; i++) {
//				grade += "☆";
//			}
		}
		
		
		//정보를 출력하는 추상메소드
		public abstract void getInformation() ;
		
			
		
		
		
		
		
		
		
		
		
}
