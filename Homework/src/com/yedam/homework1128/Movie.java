package com.yedam.homework1128;

public class Movie extends Culture{

	
	public String gerne;
	
	public Movie(String title, int supervisor, int actor, String gerne) {
		super(title,supervisor,actor);
		this.gerne = gerne;
	}
	
	
	@Override
	public void getInformation() {
		
	System.out.println(gerne + "영화제목 :" + title);
	System.out.println(gerne + "감독 수 :" + director);
	System.out.println(gerne + "배우 :" + actor);
	System.out.println(gerne + "영화 총점" + total);
	System.out.println(gerne + "영화 평점" + getGrade());

	}
	
}
