package com.yedam.work;

import java.util.Scanner;

public class Homework {
  public static void main(String[] args) {
	//11-24숙제 풀이
	  
	  
	  //최고 값 max 찾기
	  //1. 모든 제품의 합 - 최고가격 
	  //2.최고가격의 인덱스 값을 따로 저장 후 반복문을 한번 더 돌려서 해당제품제외
	  //while(){   switch문 사용도 가능
	  //메뉴 출력
	  //메뉴를 진입할 수 있도록 조건문 통해서 구별
	  //}
	  //2) ->입력한 상품 수만큼 상품명과 해당 가격을 입력받을 수 있도록 구현하세요.
	  //설계도를 만들어서 객체화, 여러가지 상품 배열
	  //상품 클래스를 만들 때 필드로는 상품명과 가격이 들어 가야한다.
	  //입력은 반복문을 활용 ( 배열의 크기만큼 반복 )
	  //3) -> 입력한 상품을 배열에 모두 들어가있는 상태를 출력
	  //출력 -> 반복문을 활용 (배열의 크기만큼) -> 각 방에 있는 객체를 하나씩 꺼내옴.
	  //객체가 가지고 있는 필드(정보를 담은 변수) ->출력예시에 맞게 만든다.
	  //4) -> 분석기능
	  //전체 합 - 최고가격
	  //5) 종료 메세지 출력 break;
	  
	  
	  //if문을 활용한 풀이
	  Scanner sc = new Scanner(System.in);
	  Product[] pd = null;
	  
	  //배열의 크기만 받는 행동을 하고 데이터를 넣을때 배열의 크기를 확장시켜주기
	  int productCount = 0;
	  
	  //바깥에 배열을 선언해주기, while안에 있으면 동작이 끝난 후 값이 날아갈수있어서
	  while(true) {
		  System.out.println("1.상품 수 || 2.상품 및 가격입력 || 3.제품별 가격 || 4.분석 || 5. 종료");
		  System.out.println("입력해주세요> ");
		  String selectNo = sc.nextLine();
		  //1번 상품수 입력
		  if(selectNo.equals("1")) {
			  System.out.println("상품 수 입력>");
			  productCount = Integer.parseInt(sc.nextLine());
		  }else if(selectNo.equals("2")) {
			  //입력받은 상품수의 수로 배열의 크기를 확정
			  pd = new Product[productCount];
			  //pd.length[5] 
			  
			  for(int i =0; i<pd.length; i++) {
				  Product product = new Product();
				  //반복문을 돌릴때마다 상품명을 입력해주기 위해서 초기화 시키기 위해 for안에 product 정의
				  System.out.println((i+1)+"번째 상품");
				  System.out.println("상품명>");
				  //변수에 데이터를 입력 받고 객체에 데이터를 넣는 방법
				  String name = sc.nextLine();
				  product.ProductName = name;
				  
				  System.out.println("상품가격을 입력>");
				  //데이터를 입력 받음과 동시에 객체에 데이터를 넣는 방법
				  product.Price = Integer.parseInt(sc.nextLine());
				  
				  pd[i] = product;
				  System.out.println("========================");
			  }
			  
		  }else if(selectNo.equals("3")) {
			  	  //베열의 크기만큼 반복문을 진행할때 배열에 각 인덱스(방 번호)를
			  	  //기입 활용하여 객체를 꺼내와서 객체 정보를 하나씩 꺼내옴.
			  	for(int i=0; i<pd.length; i++) {
			  		String name = pd[i].ProductName;
			  		//String name = product.ProductName;
			  		int price = pd[i].Price;
			  		System.out.println("상품명 : " + name + ", 상품가격 : " + price );
			  	}
		  }else if(selectNo.equals("4")) {
			  	//분석
			  	int max = pd[0].Price;
			  	int sum = 0;
			  	for(int i=0; i<pd.length; i++) {
			  		if(max<pd[i].Price) {
			  			max = pd[i].Price;
			  		}
			  		sum += pd[i].Price;
			  		
			  	}
			  	System.out.println("제일 비싼 상품 가격 : " + max);
			  	System.out.println("제일 비싼 상품을 제외한 상품 총 합 : " + (sum-max));
			  
		  }else if(selectNo.equals("5")) {
			  	System.out.println("프로그램을 종료합니다");
			  	break;
		  }
		  
		  
		  
		  
		  
		  
	  }
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
}
}
