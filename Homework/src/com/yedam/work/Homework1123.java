package com.yedam.work;

import java.util.Arrays;
import java.util.Scanner;

public class Homework1123 {
  public static void main(String[] args) {
	// TODO Auto-generated method stub
		
		
		 Scanner sc = new Scanner(System.in); 
		  
		//주어진 배열을 이용하여 다음 내용을 구현하세요.
		int[] arr1 = { 10, 60, 30, 50, 3, 100, -3 };
		int a =arr1[0];
		int b =0;
		int c =0;
		
		
		//문제1. 주어진 배열 중에서 값이 60인 곳의 인덱스를 출력해보자.
		for(int index : arr1) {
			if(index == 60) {
				c=b;
			  }	
			b++;	
		}
		System.out.println(c);
		//c
		
		for(int i=0; i<arr1.length; i++) {
			if(arr1[i] ==60) {
				b = i;
			}	
		}
		System.out.println("60의 인덱스 값은 : " + b);
		
		
		
		//문제2. 주어진 배열의 인덱스가 3인 곳은 출력하지 말고, 나머지만 출력해보자.
			  
		for(int i=0; i<arr1.length; i++) {
			
			if(arr1[i]<3 || arr1[i]>=4) {
				System.out.println(arr1[i]);
			}	
		}
		  
		
		//문제3. 주어진 배열 안의 변경하고 싶은 값의 인덱스 번호를 입력받아, 그 값을 1000으로 변경해보자.
		//   입력) 인덱스: 3 ->   {10, 20, 30, 1000, 3, 60, -3}
		  
		 
		 int ten = 1000;
		 
		 System.out.println("변경하고자 하는 인덱스값을 입력해주세요>");
		 int in = Integer.parseInt(sc.nextLine());
		 
		 //배열 데이터 넣는 방법 2개 arr1[0] = 1000;
		 arr1[in] = 1000;
		 System.out.println(arr1[in]);
		 
		 for(int i=0; i<arr1.length; i++) {
		 System.out.println(arr1[i]);
		 }  //확인용

		 
		 
		//문제4. 주어진 배열의 요소에서 최대값과 최소값을 구해보자.
		//int[] arr1 = { 10, 20, 30, 50, 3, 100, -3 };
		
		 int max = arr1[0];
		 int min = arr1[0];
		 
		 for(int i=0; i<arr1.length; i++) {
			 if(max<arr1[i]) {
				 max =arr1[i];
				 
			 }else if(min>arr1[i]) {
				 min=arr1[i];
				 
			 }
		 }
		 
		 System.out.println(max);
		 System.out.println(min);
		 
		 
		 
		//문제5. 별도의 배열을 선언하여 양의 정수 10개를 입력받아 배열에 저장하고,
		//배열에 있는 정수 중에서 3의 배수만 출력해보자.
		
		 int[] inten = new int[10];
		 System.out.println("10개의 정수를 입력해주세요>");
		
		 
		 for(int i=0; i<inten.length; i++) {
			System.out.println((i+1) +"번째 정수>");
		 	inten[i] =Integer.parseInt(sc.nextLine());
		 	
		 }
		 
		 int three = 0;
		 for(int i=0; i<inten.length; i++) {
			 if(inten[i]%3 == 0 && inten[i]>0) {
				 System.out.println("3의배수 : " + inten[i]);
			 }else {
				 System.out.println("3의 배수가 없습니다.");
				 break;
			 }
		 }
		 
		
	
		 
		 
		 //추가문제   로또생성하기
		 int[] lotnum=new int [6];
		 int mynum[] = new int [6];
		 
		 for(int i=0; i<6; i++) {
			 System.out.println((i+1) + "번째 숫자를 입력해주세요>");
			
			 mynum[i]= Integer.parseInt(sc.nextLine());
			 lotnum[i] = (int)(Math.random()* 45)+1;
			 for(int j=0; j<i; j++ ) {
				  if(lotnum[i] == lotnum[j]) {
					  i--;
					  break;
				  }//중복제거
			  }
		 }
		 
		 					  					 
		 System.out.print("로또번호");
		 System.out.println(Arrays.toString(lotnum));   //배열 전체의 값 출력
		 System.out.println(Arrays.toString(mynum));    //응모번호
		 
		 
		  

}
}
