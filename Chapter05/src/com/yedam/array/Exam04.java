package com.yedam.array;

import java.util.Scanner;

public class Exam04 {
  
	public static void main(String[] args) {
		//Lotto
		//1~45 랜덤으로 6번의 숫자 입력- 당첨여부 
		//랜덤의 숫자 6개 필요.
		//중복을 막아야 함 
		
		//배열에 담긴 값 중에서 최대값, 최소값 구하기
		
		Scanner sc = new Scanner(System.in);  
		
		int[] ary;
		int no;
		
		System.out.println("배열의 크기>");
		no = Integer.parseInt(sc.nextLine());
		ary = new int[no];
		
		for(int i=0; i<ary.length; i++) {
			System.out.println("입력>");
			ary[i] = Integer.parseInt(sc.nextLine());
		}
		
		
		
		//최대값 구하기
		int max = 0;
		for(int i=0; i<ary.length; i++) {
			if(max< ary[i]) {
				max = ary[i];
			}
			
		}
		
		System.out.println("최대값 :" + max);
		
		
		
		int min = ary[0];   //기준을 입력받은 숫자의 값을 넣어줌
		for(int i=0; i< ary.length; i++) {
		
			if(min > ary[i]) {
				min = ary[i];
			}
		}
		System.out.println("최소값 :" + min);
		
		
		
		
		
		
		
		
		
		
	}	
}
