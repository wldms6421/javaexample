package com.yedam.array;

import java.util.Scanner;

public class Exam05 {
  public static void main(String[] args) {
	boolean run = true;
	int studentNum = 0;    //학생수
	int[] score = null;    //입력받은 점수
	Scanner scanner = new Scanner(System.in);
	
	while(run) {
		System.out.println("============================");
		System.out.println("1.학생수|2.점수입력|3.점수리스트|4.분석|5.종료");
		//학생수 = 배열의 크기,
		System.out.println("선택 > ");
		
		int selectNo = Integer.parseInt(scanner.nextLine());
		
		
		
			if(selectNo ==1) {//학생수- 배열의 크기로 만들어
				
				System.out.println("학생수를 입력하세요>");
				studentNum = Integer.parseInt(scanner.nextLine());
				System.out.println("학생수는" +studentNum+ "입니다." );
			}else if(selectNo ==2) {//점수입력
				score = new int[studentNum];
				for(int i=0; i<score.length; i++) {	
					System.out.println("score[" + i+ "]" + ">");
					score[i] = Integer.parseInt(scanner.nextLine());
				}
					
			}else if(selectNo ==3) {//점수리스트
				
				for(int i=0; i<score.length; i++) {
				System.out.println("score [" + i + "] " +score[i]);
				}
				
			}else if(selectNo ==4) {//분석
				
				int sum=0;
				int max = score[0];
				
				for(int i=1; i<score.length; i++) {
					if(max<score[i]) {
						max = score[i];
					}
					sum = sum+ score[i];
				}
				System.out.println("최고 점수>" + max);
				System.out.println("평균 점수>" + ((double)sum/score.length));
			}else if(selectNo ==5) {//종료
				run = false;
			}
			
		
		
		
		
	}//while
	  
	  System.out.println("프로그램 종료");
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
  }
}
