package com.yedam.array;

public class Exam06 {
	  public static void main(String[] args) {
		//최대값의 인덱스 구하기. (최대값이 존재하는 방 번호 구하기)
		 
		  int[] array = {10,50,70,20,30,80,40};
		  int max = array[0];
		  int maxindex = 0;
		  for (int i=0; i<array.length; i++) {
			  if(max<array[i]) {
				  max = array[i];	
				  maxindex = i;
			  }			  
		  }
		  
		  System.out.println("최대값" + max);
		  System.out.println("최대값이 있는 인덱스의 주소>" + maxindex);
		  
		  
		  
		  
		  
		  
		  
	  }
}
