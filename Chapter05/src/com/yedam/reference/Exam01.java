package com.yedam.reference;

public class Exam01 {
  public static void main(String[] args) {
	int intVal = 10;                 //10
	int[] array = {1,2,3,4,5,6};        //array의 주소값이 나옴
	//[I@2401f4c3 heap에 주소값이 있어서 참조해서 써야함
	int[] array2 = {1,2,3,4,5,6};
	//int[] array3; //null이 들어가 있는 상태.
	//int a; NullPointException
	
	System.out.println(intVal);
	System.out.println(array);
	System.out.println(array2);
	System.out.println(array == array2);    //false 주소의 값이 달라서, 데이터의 값을 비교해주는 것 x
	//System.out.println(array3); 에러남 
	//System.out.println(a);
	
	
	
  }
}
