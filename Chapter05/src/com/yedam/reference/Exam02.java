package com.yedam.reference;

public class Exam02 {
  public static void main(String[] args) {
	String strVal1 = "yedam";
	String strVal2 = "yedam";
	
	
	//참조타입의 ==는 데이터를 비교 x, 데이터 메모리 주소를 비교
	if(strVal1 == strVal2) {   
		System.out.println("strVal1과 strVal2는 메모리 주소 같다");
	}else {
		System.out.println("strVal1과 strVal2는 메모리 주소 다르다");
	}
	
	//데이터가 같은지 비교
	if(strVal1.equals(strVal2)) {
		System.out.println("strVal1과 strVal2는 데이터는 같다");
	}else {
		System.out.println("strVal1과 strVal2는 데이터는 다르다");
	}
	
	  
	String strVal3 = new String("yedam");
	String strVal4 = new String("yedam");
	
	if(strVal3 == strVal4) {  //주소를 비교
		System.out.println("strVal3과 strVal4는 같은 메모리 주소를 가지고 있다.");
	}else {
		System.out.println("strVal3과 strVal4는 다른 메모리 주소를 가지고 있다.");
	}
	
	
	if(strVal3.equals(strVal4)) {
		System.out.println("strVal3과 strVal4는 데이터는 같다.");
	}else {
		System.out.println("strVal3과 strVal4는 데이터는 다르다.");
	}
	
	
	if(strVal1 == strVal3) {
		System.out.println("strVal1과 strVal3은 같은 메모리 주소를 가지고 있다.");
	}else {
		System.out.println("strVal1과 strVal3은 다른 메모리 주소를 가지고 있다.");
	}// strVal3 은 new 연산자를 이용하여 독자적인 주소를 생성했기 때문에 다른 주소
	
	
	
}
}
