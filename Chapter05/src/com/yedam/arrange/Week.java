package com.yedam.arrange;

public enum Week {
	//자바의 상수는 대문자로 사용, 변하지 않는 값
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY
}
